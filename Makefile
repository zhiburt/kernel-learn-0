# A LOT of quirks is done in this file, that's probably why mature kernels split makefiles per dir :)
# todo: _

CC 			:= gcc
CFLAGS 		:= -Wall
CKFLAGS 	:= -m32 -ffreestanding --no-pie -fno-stack-protector
INC 		:=

AS 			:= nasm

SRCDIR			:= src
BUILDDIR		:= build
OBJECTDIR		:= build/obj
INCDIR			:= include
TARGETDIR		:= build/target

BOOTDIR			:= boot

CFILES			:= $(shell find . -not -path "./multiboot/*" -type f -name *.c)
ASMFILES		:= $(shell find . -not -path "./boot/*" -not -path "./multiboot/*" -type f -name *.asm)

COBJECTS		:= $(foreach file, $(CFILES:.c=.o), $(addprefix $(shell echo $(OBJECTDIR)), $(subst ./,/,$(file))))
ASMOBJECTS		:= $(foreach file, $(ASMFILES:.asm=.o), $(addprefix $(shell echo $(OBJECTDIR)), $(subst ./,/,$(file))))
OBJECTS			:= $(COBJECTS) $(ASMOBJECTS)

BOOTFILES		:= $(shell find ./boot/ -type f -name *.asm)

.PHONY: all
all: run

run-grub: $(BUILDDIR)/os.iso
	qemu-system-i386 --cdrom $<

run: $(BUILDDIR)/os-image.bin
	qemu-system-i386 -fda $<

$(BUILDDIR)/os.iso: multiboot/
	mkdir -p $(OBJECTDIR)/multiboot
	$(AS) -i./multiboot/multiboot2/ -f elf32 -o $(OBJECTDIR)/multiboot/main.o multiboot/multiboot2/main.asm
	$(CC) $(CFLAGS) $(CKFLAGS) $(INC) -c -o $(OBJECTDIR)/multiboot/kmain.o multiboot/multiboot2/main.c

	ld -m elf_i386 -n -T multiboot/multiboot2/linker.ld -o $(BUILDDIR)/os.bin $(OBJECTDIR)/multiboot/main.o $(OBJECTDIR)/multiboot/kmain.o

	mkdir -p $(BUILDDIR)/isodir/boot/grub
	cp $(BUILDDIR)/os.bin $(BUILDDIR)/isodir/boot/os.bin
	cp multiboot/multiboot2/grub.cfg $(BUILDDIR)/isodir/boot/grub/grub.cfg
	grub-mkrescue -o $(BUILDDIR)/os.iso $(BUILDDIR)/isodir

$(BUILDDIR)/os-image.bin: $(TARGETDIR)/boot.bin $(TARGETDIR)/kernel.bin
	cat $^ > $@

$(TARGETDIR)/kernel.bin: $(OBJECTS)
# could be resolved by a .ld file and a custom .section
	mkdir -p $(TARGETDIR)
	mkdir -p $(OBJECTDIR)/boot
	$(AS) -i./boot/ -f elf32 -o $(OBJECTDIR)/boot/kernel-entry.o $(BOOTDIR)/kernel-entry.asm
	ld -m elf_i386 -o $(TARGETDIR)/kernel.o -r $(OBJECTDIR)/boot/kernel-entry.o $^
	ld -m elf_i386 -o $@ -Ttext 0x1000 $(TARGETDIR)/kernel.o --oformat binary

$(OBJECTDIR)/%.o: $(CFILES)
	mkdir -p $(dir $@)
	$(CC) $(CFLAGS) $(CKFLAGS) $(INC) -c -o $@ $(patsubst %.o,%.c, $(subst $(OBJECTDIR),.,$@))

$(ASMOBJECTS): $(ASMFILES)
	mkdir -p $(dir $@)
	$(AS) -i./boot/ -f elf32 -o $@ $(patsubst %.o,%.asm, $(subst $(OBJECTDIR),.,$@))

$(TARGETDIR)/boot.bin: $(BOOTFILES)
	mkdir -p $(TARGETDIR)
	$(AS) -i$(BOOTDIR) -f bin -o $@ $(BOOTDIR)/mbr.asm

.PHONY: clean
clean:
	$(RM) -r $(BUILDDIR)
