[org 0x7c00]
[bits 16]

; where to load the kernel to
KERNEL_OFFSET equ 0x1000

; BIOS sets boot drive in 'dl'; store for later use
mov [BOOT_DRIVE], dl

; setup stack
mov bp, 0x9000
mov sp, bp

; mov ax, 4F02h
; mov bx, 107h
; int 10h

mov bx, MSG_16BIT_MODE
call print16
call print16_nl

call load_kernel
call switch_to_32bit

jmp $

%include "print-16bit.asm"
%include "print-32bit.asm"
%include "disk.asm"
%include "gdt.asm"
%include "switch-to-32bit.asm"

[bits 16]
load_kernel:
    mov bx, KERNEL_OFFSET ; bx -> destination
    mov dh, 31             ; dh -> num sectors
    mov dl, [BOOT_DRIVE]  ; dl -> disk
    call disk_load
    ret

[bits 32]
BEGIN_32BIT:
    mov ebx, MSG_32BIT_MODE
    call print32
    push 0
    push 0
    call KERNEL_OFFSET  ; give control to the kernel
    jmp $               ; loop in case kernel returns

; boot drive variable
BOOT_DRIVE db 0
MSG_16BIT_MODE db "Started int 16-bit Real Mode", 0
MSG_32BIT_MODE db "Getting into to 32-bit Protected Mode", 0

; padding
times 510 - ($-$$) db 0

; magic number
dw 0xAA55

; Our stack area.

; %define STACK_SIZE  0x4000

; section bss
; align  16 ; Accoding to System-V ABI
; stack_bottom:
; stack:	times STACK_SIZE db 0
; stack_top:
