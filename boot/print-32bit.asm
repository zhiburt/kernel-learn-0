[bits 32]

VGA_ADDR equ 0xb8000
CLR_WHITE_ON_BLACK equ 0x0f

print32:
    pusha
    mov edx, VGA_ADDR

.loop:
    mov al, [ebx]
    mov ah, CLR_WHITE_ON_BLACK

    cmp al, 0
    je .done

    mov [edx], ax
    add ebx, 1
    add edx, 2

    jmp .loop

.done:
    popa
    ret
