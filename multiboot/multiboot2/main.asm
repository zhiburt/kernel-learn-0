%define STACK_SIZE							0x4000

%define MULTIBOOT2_HEADER_MAGIC				0xe85250d6
%define MULTIBOOT_HEADER_TAG_ADDRESS		2
%define MULTIBOOT_HEADER_TAG_OPTIONAL		1
%define MULTIBOOT_HEADER_TAG_ENTRY_ADDRESS	3
%define MULTIBOOT_HEADER_TAG_FRAMEBUFFER	5
%define MULTIBOOT_HEADER_TAG_END			0

%define GRUB_MULTIBOOT_ARCHITECTURE_I386	0

[bits 32]

extern kmain, printf
extern edata, end

section .multiboot
align 4
multiboot_header:
        ; magic
        dd   MULTIBOOT2_HEADER_MAGIC
        ; ISA: i386
        dd   GRUB_MULTIBOOT_ARCHITECTURE_I386
        ; Header length.
        dd   multiboot_header_end - multiboot_header
        ; checksum
        dd   -(MULTIBOOT2_HEADER_MAGIC + GRUB_MULTIBOOT_ARCHITECTURE_I386 + (multiboot_header_end - multiboot_header))
; address_tag_start:
;         dw	MULTIBOOT_HEADER_TAG_ADDRESS
;         dw	MULTIBOOT_HEADER_TAG_OPTIONAL
;         dd	address_tag_end - address_tag_start
;         ; header_addr
;         dd	multiboot_header
;         ; load_addr
;         dd	_start
;         ; load_end_addr
;         dd	edata
;         ; bss_end_addr
;         dd	end
; address_tag_end:
; entry_address_tag_start:        
;         dw MULTIBOOT_HEADER_TAG_ENTRY_ADDRESS
;         dw MULTIBOOT_HEADER_TAG_OPTIONAL
;         dd entry_address_tag_end - entry_address_tag_start
;         ; entry_addr
;         dd _start
; entry_address_tag_end:
; framebuffer_tag_start:  
;         dw MULTIBOOT_HEADER_TAG_FRAMEBUFFER
;         dw MULTIBOOT_HEADER_TAG_OPTIONAL
;         dd framebuffer_tag_end - framebuffer_tag_start
;         dd 1024
;         dd 768
;         dd 32
; framebuffer_tag_end:
        dw MULTIBOOT_HEADER_TAG_END
        dw 0
        dd 8
multiboot_header_end:

section .text
global  _start

%include "gdt.asm"

_start:
        ; Initialize the stack pointer.
        mov	esp, stack_top

        ; Reset EFLAGS.
        push 0
        popf

        ; Push the pointer to the Multiboot information structure.
        push ebx
        ; Push the magic value.
        push eax

	    lgdt [gdt_descriptor]   ; load the GDT descriptor

        ; ; Now enter the C main function...
        call kmain

        ; Halt.
        ; mov ebx, HALT_MESSAGE
        ; call	print32

loop:   hlt
        jmp     loop

HALT_MESSAGE db "XXXXXXXXXXXXXXXXX", 0

VGA_ADDR equ 0xb8000
CLR_WHITE_ON_BLACK equ 0x0f

print32:
    pusha
    mov edx, VGA_ADDR

.loop:
    mov al, [ebx]
    mov ah, CLR_WHITE_ON_BLACK

    cmp al, 0
    je .done

    mov [edx], ax
    add ebx, 1
    add edx, 2

    jmp .loop

.done:
    popa
    ret

; Our stack area.
section bss
align  16 ; Accoding to System-V ABI
stack_bottom:
stack:	times STACK_SIZE db 0
stack_top:
