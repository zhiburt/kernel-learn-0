#ifndef __BITS__
#define __BITS__

#define BITS_LOW16(n) ((uint16_t)(n & 0xFFFF))
#define BITS_HIGH16(n) ((uint16_t)((n >> 16) & 0xFFFF))

#endif