#ifndef __STRING__
#define __STRING__

int string_length(char *s);
int string_cmp(char *a, char *b);
void string_append(char *s, char c);

#endif