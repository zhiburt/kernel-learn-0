int
string_length(const char *s)
{
	int i = 0;
	for (; *s != '\0'; s++) {
		i++;
	}

	return (i);
}

void
string_append(char *s, char c)
{
	int len = string_length(s);
	s[len] = c;
	s[len + 1] = '\0';
}

int
string_cmp(const char *a, const char *b)
{
	for (; *a == *b; a++, b++) {
		if (*a == '\0') {
			return (0);
		}
	}

	return (*a - *b);
}
