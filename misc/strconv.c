int
int_length(int n)
{
	int size = 1;

	while ((n /= 10) > 0) {
		size += 1;
	}

	return (size);
}

void
int_to_string(int n, char *buf)
{
	int i = 0, sign = 0, length = 0;

	if (n < 0) {
		sign = 1;
		n = -n;
	}

	length = int_length(n) + sign;
	i = length;

	while (n > 9) {
		buf[--i] = n % 10 + '0';
		n /= 10;
	}

	buf[--i] = n % 10 + '0';

	if (sign)
		buf[0] = '-';

	buf[length] = '\0';
}
