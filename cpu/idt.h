#ifndef __IDT__
#define __IDT__

#include <stdint.h>

#define KERNEL_CS 0x08
#define IDT_ENTRIES 256

typedef struct {
	uint16_t low_offset;
	uint16_t seg_selector;
	uint8_t unused0;
	uint8_t flags;
	uint16_t high_offset;
} __attribute__((packed)) idt_gate_t;

typedef struct {
	uint16_t limit;
	uint16_t base;
} __attribute__((packed)) idt_register_t;

void idt_set_gate(int n, uint32_t handler);
void idt_load();

#endif