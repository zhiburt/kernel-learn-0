#include <stdint.h>

#include "../misc/bits.h"
#include "idt.h"

idt_gate_t idt[IDT_ENTRIES];
idt_register_t idt_reg;

void
idt_set_gate(int n, uint32_t handler)
{
	idt_gate_t *entry = &idt[n];

	entry->low_offset = BITS_LOW16(handler);
	entry->seg_selector = KERNEL_CS;
	entry->unused0 = 0;
	entry->flags = 0x8E;
	entry->high_offset = BITS_HIGH16(handler);
}

void
idt_load()
{
	idt_reg.base = (uint32_t)(uintptr_t)&idt;
	idt_reg.limit = IDT_ENTRIES * sizeof(idt_gate_t) - 1;

	__asm__ volatile("lidt (%0)" ::"r"(&idt_reg));
}
