#include <stdint.h>

#include "../drivers/display.h"
#include "../drivers/ports.h"
#include "../misc/strconv.h"
#include "idt.h"
#include "isr.h"

isr_t interrupt_handlers[256];

static char *exception_messages[] = { "Division By Zero", "Debug",
	"Non Maskable Interrupt", "Breakpoint", "Into Detected Overflow",
	"Out of Bounds", "Invalid Opcode", "No Coprocessor",

	"Double Fault", "Coprocessor Segment Overrun", "Bad TSS",
	"Segment Not Present", "Stack Fault", "General Protection Fault",
	"Page Fault", "Unknown Interrupt",

	"Coprocessor Fault", "Alignment Check", "Machine Check", "Reserved",
	"Reserved", "Reserved", "Reserved", "Reserved",

	"Reserved", "Reserved", "Reserved", "Reserved", "Reserved", "Reserved",
	"Reserved", "Reserved" };

void
isr_install()
{
	idt_set_gate(0, (uint32_t)(uintptr_t)isr0);
	idt_set_gate(1, (uint32_t)(uintptr_t)isr1);
	idt_set_gate(2, (uint32_t)(uintptr_t)isr2);
	idt_set_gate(3, (uint32_t)(uintptr_t)isr3);
	idt_set_gate(4, (uint32_t)(uintptr_t)isr4);
	idt_set_gate(5, (uint32_t)(uintptr_t)isr5);
	idt_set_gate(6, (uint32_t)(uintptr_t)isr6);
	idt_set_gate(7, (uint32_t)(uintptr_t)isr7);
	idt_set_gate(8, (uint32_t)(uintptr_t)isr8);
	idt_set_gate(9, (uint32_t)(uintptr_t)isr9);
	idt_set_gate(10, (uint32_t)(uintptr_t)isr10);
	idt_set_gate(11, (uint32_t)(uintptr_t)isr11);
	idt_set_gate(12, (uint32_t)(uintptr_t)isr12);
	idt_set_gate(13, (uint32_t)(uintptr_t)isr13);
	idt_set_gate(14, (uint32_t)(uintptr_t)isr14);
	idt_set_gate(15, (uint32_t)(uintptr_t)isr15);
	idt_set_gate(16, (uint32_t)(uintptr_t)isr16);
	idt_set_gate(17, (uint32_t)(uintptr_t)isr17);
	idt_set_gate(18, (uint32_t)(uintptr_t)isr18);
	idt_set_gate(19, (uint32_t)(uintptr_t)isr19);
	idt_set_gate(20, (uint32_t)(uintptr_t)isr20);
	idt_set_gate(21, (uint32_t)(uintptr_t)isr21);
	idt_set_gate(22, (uint32_t)(uintptr_t)isr22);
	idt_set_gate(23, (uint32_t)(uintptr_t)isr23);
	idt_set_gate(24, (uint32_t)(uintptr_t)isr24);
	idt_set_gate(25, (uint32_t)(uintptr_t)isr25);
	idt_set_gate(26, (uint32_t)(uintptr_t)isr26);
	idt_set_gate(27, (uint32_t)(uintptr_t)isr27);
	idt_set_gate(28, (uint32_t)(uintptr_t)isr28);
	idt_set_gate(29, (uint32_t)(uintptr_t)isr29);
	idt_set_gate(30, (uint32_t)(uintptr_t)isr30);
	idt_set_gate(31, (uint32_t)(uintptr_t)isr31);

	port_write_dd(0x20, 0x11);
	port_write_dd(0xA0, 0x11);
	port_write_dd(0x21, 0x20);
	port_write_dd(0xA1, 0x28);
	port_write_dd(0x21, 0x04);
	port_write_dd(0xA1, 0x02);
	port_write_dd(0x21, 0x01);
	port_write_dd(0xA1, 0x01);
	port_write_dd(0x21, 0x0);
	port_write_dd(0xA1, 0x0);

	idt_set_gate(32, (uint32_t)(uintptr_t)irq0);
	idt_set_gate(33, (uint32_t)(uintptr_t)irq1);
	idt_set_gate(34, (uint32_t)(uintptr_t)irq2);
	idt_set_gate(35, (uint32_t)(uintptr_t)irq3);
	idt_set_gate(36, (uint32_t)(uintptr_t)irq4);
	idt_set_gate(37, (uint32_t)(uintptr_t)irq5);
	idt_set_gate(38, (uint32_t)(uintptr_t)irq6);
	idt_set_gate(39, (uint32_t)(uintptr_t)irq7);
	idt_set_gate(40, (uint32_t)(uintptr_t)irq8);
	idt_set_gate(41, (uint32_t)(uintptr_t)irq9);
	idt_set_gate(42, (uint32_t)(uintptr_t)irq10);
	idt_set_gate(43, (uint32_t)(uintptr_t)irq11);
	idt_set_gate(44, (uint32_t)(uintptr_t)irq12);
	idt_set_gate(45, (uint32_t)(uintptr_t)irq13);
	idt_set_gate(46, (uint32_t)(uintptr_t)irq14);
	idt_set_gate(47, (uint32_t)(uintptr_t)irq15);

	idt_load();
}

void
isr_handler(registers_t *r)
{
	char code[3];

	print_string("received interrupt: ");
	int_to_string(r->interrupt, code);
	print_string(code);
	print_nl();
	print_string(exception_messages[r->err]);
	print_nl();
}

void
isr_handler_register(uint8_t n, isr_t handler)
{
	interrupt_handlers[n] = handler;
}

void
irq_handler(registers_t *r)
{
	isr_t handler = interrupt_handlers[r->interrupt];

	if (handler != 0) {
		handler(r);
	}

	if (r->interrupt >= 40) {
		port_write_dd(0xA0, 0x20);
	}

	port_write_dd(0x20, 0x20);
}