#include "../cpu/isr.h"
#include "../drivers/display.h"
#include "../drivers/keyboard.h"
#include "../misc/strconv.h"
#include "../sys/vm/uma.h"
#include "../multiboot/multiboot2/multiboot2.h"

void
test_allocator()
{
	void *ptr1 = (void *)uma_alloc(5);
	uma_debug_mem();

	void *ptr2 = (void *)uma_alloc(400);
	uma_debug_mem();

	void *ptr3 = (void *)uma_alloc(3000);
	uma_debug_mem();

	uma_free(ptr3);
	uma_debug_mem();

	uma_free(ptr1);
	uma_debug_mem();

	uma_free(ptr2);
	uma_debug_mem();
}

int
kmain(unsigned long magic, unsigned long addr)
{
	// char buf[100] = {0};

	clear_screen();

	/*  Am I booted by a Multiboot-compliant boot loader? */
	// if (magic != MULTIBOOT2_BOOTLOADER_MAGIC) {
	// 	int_to_string(magic, buf);
	// 	print_string("Invalid magic number");
	// 	print_string(" ");
	// 	print_string(buf);
	// 	print_string("\n");
	// 	// return (-1);
	// }

	// if (addr & 7) {
	// 	int_to_string(magic, buf);
	// 	print_string("Unaligned mbi");
	// 	print_string(" ");
	// 	print_string(buf);
	// 	print_string("\n");
	// 	// return (-1);
	// }

	print_string("Installing interrupt service routines (ISRs)\n");
	isr_install();

	print_string("Enabling interrupts\n");
	__asm__ volatile("sti");

	print_string("Setting up an Allocator\n");
	uma_init();

	test_allocator();

	print_string("Initializing keyboard (IRQ 1).\n");
	init_keyboard();

	print_string("> ");

	return (0);
}
