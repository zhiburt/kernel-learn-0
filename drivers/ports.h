#ifndef __PORTS__
#define __PORTS__

#include <stdint.h>

uint8_t port_read_dd(uint16_t port);
void port_write_dd(uint16_t port, uint8_t data);

uint16_t port_read_dw(uint16_t port);
void port_write_dw(uint16_t port, uint16_t data);

#endif