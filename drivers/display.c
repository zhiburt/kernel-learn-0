#include <stdint.h>

#include "display.h"
#include "../misc/mem.h"
#include "ports.h"

#define VIDEO_ADDRESS 0xb8000
#define MAX_ROWS 25
#define MAX_COLS 80
#define WHITE_ON_BLACK 0x0f

#define REG_SCREEN_CTRL 0x3d4
#define REG_SCREEN_DATA 0x3d5

void set_cursor(int offset) {
  offset /= 2;
  port_write_dd(REG_SCREEN_CTRL, 14);
  port_write_dd(REG_SCREEN_DATA, (unsigned char)(offset >> 8));
  port_write_dd(REG_SCREEN_CTRL, 15);
  port_write_dd(REG_SCREEN_DATA, (unsigned char)(offset & 0xff));
}

int get_cursor() {
  port_write_dd(REG_SCREEN_CTRL, 14);
  int offset = port_read_dd(REG_SCREEN_DATA) << 8; /* High byte: << 8 */
  port_write_dd(REG_SCREEN_CTRL, 15);
  offset += port_read_dd(REG_SCREEN_DATA);
  return offset * 2;
}

int get_offset(int col, int row) { return 2 * (row * MAX_COLS + col); }

int get_row_from_offset(int offset) { return offset / (2 * MAX_COLS); }

int move_offset_to_new_line(int offset) {
  return get_offset(0, get_row_from_offset(offset) + 1);
}

void set_char_at_video_memory(char character, int offset) {
  char *vidmem = (char *)VIDEO_ADDRESS;
  vidmem[offset] = character;
  vidmem[offset + 1] = WHITE_ON_BLACK;
}

int scroll_ln(int offset) {
  char *dst = (char *)((uintptr_t)VIDEO_ADDRESS + get_offset(0, 0));
  char *src = (char *)((uintptr_t)VIDEO_ADDRESS + get_offset(0, 1));

  mem_cpy(dst, src, MAX_COLS * (MAX_ROWS - 1) * 2);

  for (int col = 0; col < MAX_COLS; col++) {
    set_char_at_video_memory(' ', get_offset(col, MAX_ROWS - 1));
  }

  return offset - 2 * MAX_COLS;
}

/*
 * TODO:
 * - handle illegal offset (print error message somewhere)
 */
void print_string(char *string) {
  int offset = get_cursor();
  int i = 0;
  while (string[i] != '\0') {
    if (offset >= MAX_ROWS * MAX_COLS * 2) {
      offset = scroll_ln(offset);
    }

    if (string[i] == '\n') {
      offset = move_offset_to_new_line(offset);
    } else {
      set_char_at_video_memory(string[i], offset);
      offset += 2;
    }

    i++;
  }

  set_cursor(offset);
}

void print_nl() {
  int newOffset = move_offset_to_new_line(get_cursor());
  if (newOffset >= MAX_ROWS * MAX_COLS * 2) {
    newOffset = scroll_ln(newOffset);
  }
  set_cursor(newOffset);
}

void clear_screen() {
  int screen_size = MAX_COLS * MAX_ROWS;
  for (int i = 0; i < screen_size; ++i) {
    set_char_at_video_memory(' ', i * 2);
  }
  set_cursor(get_offset(0, 0));
}

void print_backspace() {
  int newCursor = get_cursor() - 2;
  set_char_at_video_memory(' ', newCursor);
  set_cursor(newCursor);
}
