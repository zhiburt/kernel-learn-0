#ifndef __DISPLAY__
#define __DISPLAY__

void print_string(char *string);
void print_nl();
void clear_screen();
int scroll_ln(int offset);
void print_backspace();

void set_cursor(int offset);
int get_cursor();
void set_char_at_video_memory(char character, int offset);

#endif