#include <stdint.h>

#include "../cpu/isr.h"
#include "../misc/string.h"
#include "display.h"
#include "keyboard.h"
#include "ports.h"

#define SC_BACKSPACE 0x0E
#define SC_ENTER 0x1C
#define SC_SHIFT_L_PRESSED 0x2A
#define SC_SHIFT_L_RELEASED 0xAA
#define SC_ALT_L_PRESSED 0x38
#define SC_ALT_L_RELEASED 0xB8
#define SC_CTRL_L_PRESSED 0x1D
#define SC_CTRL_L_RELEASED 0x9D

#define SC_LENGTH 58
#define KEYBOARD_BUFFER_LEN 256

static const char scancode_to_char_uppercase[SC_LENGTH] = { '?', '?', '1', '2',
	'3', '4', '5', '6', '7', '8', '9', '0', '-', '=', '?', '?', 'Q', 'W',
	'E', 'R', 'T', 'Y', 'U', 'I', 'O', 'P', '[', ']', '?', '?', 'A', 'S',
	'D', 'F', 'G', 'H', 'J', 'K', 'L', ';', '\'', '`', '?', '\\', 'Z', 'X',
	'C', 'V', 'B', 'N', 'M', ',', '.', '/', '?', '?', '?', ' ' };

static const char scancode_to_char_lowercase[SC_LENGTH] = { '?', '?', '1', '2',
	'3', '4', '5', '6', '7', '8', '9', '0', '-', '=', '?', '?', 'q', 'w',
	'e', 'r', 't', 'y', 'u', 'i', 'o', 'p', '[', ']', '?', '?', 'a', 's',
	'd', 'f', 'g', 'h', 'j', 'k', 'l', ';', '\'', '`', '?', '\\', 'z', 'x',
	'c', 'v', 'b', 'n', 'm', ',', '.', '/', '?', '?', '?', ' ' };

static char key_buffer[KEYBOARD_BUFFER_LEN] = { 0 };

enum {
	KEYBOARD_FLAGS_NONE = 1 << 0,
	KEYBOARD_FLAGS_SHIFT = 1 << 1,
	KEYBOARD_FLAGS_CTRL = 1 << 2,
	KEYBOARD_FLAGS_ALT = 1 << 3,
} keyboard_flags = 0;

static int
backspace(char *buf)
{
	int len = string_length(buf);
	if (len > 0) {
		buf[len - 1] = '\0';
		return (1);
	} else {
		return (0);
	}
}

static void
exec(char *input)
{
	if (string_cmp(input, "EXIT") == 0) {
		print_string("Stopping the CPU. Bye!\n");
		__asm__ volatile("hlt");
		return;
	}

	if (string_cmp(input, "CLEAR") == 0) {
		clear_screen();
		print_string("\n> ");
		return;
	}

	print_string("Unknown command: ");
	print_string(input);
	print_string("\n> ");
}

static void
keyboard_callback(registers_t *regs)
{
	char letter, str[2];
	int is_release;
	uint8_t scancode = port_read_dd(0x60);

	switch (scancode) {
	case SC_ENTER:
		print_nl();
		exec(key_buffer);
		key_buffer[0] = '\0';
		break;
	case SC_BACKSPACE:
		if (backspace(key_buffer)) {
			print_backspace();
		}

		break;
	case SC_SHIFT_L_PRESSED:
		keyboard_flags |= KEYBOARD_FLAGS_SHIFT;
		break;
	case SC_ALT_L_PRESSED:
		keyboard_flags |= KEYBOARD_FLAGS_ALT;
		break;
	case SC_CTRL_L_PRESSED:
		keyboard_flags |= KEYBOARD_FLAGS_CTRL;
		break;
	case SC_SHIFT_L_RELEASED:
		keyboard_flags &= ~KEYBOARD_FLAGS_SHIFT;
		break;
	case SC_ALT_L_RELEASED:
		keyboard_flags &= ~KEYBOARD_FLAGS_ALT;
		break;
	case SC_CTRL_L_RELEASED:
		keyboard_flags &= ~KEYBOARD_FLAGS_CTRL;
		break;
	default:
		is_release = scancode >= 0x80;
		if (scancode >= SC_LENGTH && is_release) {
			return;
		}

		if (keyboard_flags & KEYBOARD_FLAGS_SHIFT) {
			letter = scancode_to_char_uppercase[(int)scancode];
		} else {
			letter = scancode_to_char_lowercase[(int)scancode];
		}

		str[0] = letter;
		str[1] = '\0';

		print_string(str);
		string_append(key_buffer, letter);

		break;
	}
}

void
init_keyboard()
{
	isr_handler_register(IRQ1, keyboard_callback);
}