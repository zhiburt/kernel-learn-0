#include "stdint.h"
#include "stddef.h"
#include "stdint.h"

#include "uma.h"
#include "../../misc/string.h"
#include "../../misc/strconv.h"
#include "../../drivers/display.h"

// todo: Make struct public so allocators could be defined whether nessary.

#define MEM_TOTAL_SIZE 4 * 1024
#define MEM_NODE_SIZE sizeof(struct mem_node)

struct mem_node {
	uint32_t size;
	int used;
	struct mem_node *next;
	struct mem_node *prev;
};

static uint8_t mem_area[MEM_TOTAL_SIZE];
static struct mem_node *mem_root;

void *
find_best_mem_block(struct mem_node *node, size_t size)
{
	struct mem_node *current_node = (struct mem_node *)NULL_PTR;
	struct mem_node *best_node = (struct mem_node *)NULL_PTR;
	uint32_t best_mem_size = MEM_TOTAL_SIZE + 1;
	int is_smaller_block = 0;

	current_node = node;
	while (current_node) {
		is_smaller_block = (current_node->size >=
				       (size + MEM_NODE_SIZE)) &&
		    (current_node->size <= best_mem_size);
		if (!current_node->used && is_smaller_block) {
			best_node = current_node;
			best_mem_size = current_node->size;
		}

		current_node = current_node->next;
	}

	return (best_node);
}

void *
node_merge_next(struct mem_node *node)
{
	struct mem_node *next = node->next;
	if (next == NULL_PTR || next->used) {
		return (NULL_PTR);
	}

	node->size += next->size + MEM_NODE_SIZE;
	node->next = next->next;

	if (node->next != NULL_PTR) {
		node->next->prev = node;
	}

	return (node);
}

void
uma_init()
{
	mem_root = (struct mem_node *)mem_area;
	mem_root->size = MEM_TOTAL_SIZE - MEM_NODE_SIZE;
	mem_root->next = NULL_PTR;
	mem_root->prev = NULL_PTR;
}

void *
uma_alloc(size_t size)
{
	struct mem_node *next = (struct mem_node *)NULL_PTR;
	struct mem_node *block = (struct mem_node *)
	    find_best_mem_block(mem_root, size);

	if (block == NULL_PTR) {
		return (NULL_PTR);
	}

	block->size -= size + MEM_NODE_SIZE;

	next = (struct mem_node *)(((uint8_t *)block) + MEM_NODE_SIZE +
	    block->size);
	next->size = size;
	next->used = 1;
	next->next = block->next;
	next->prev = block;

	if (block->next != NULL_PTR) {
		block->next->prev = next;
	}

	block->next = next;

	return ((void *)((uint8_t *)next + MEM_NODE_SIZE));
}

int
uma_free(void *p)
{
	struct mem_node *node;

	if (p == NULL_PTR) {
		return 1;
	}

	node = (struct mem_node*) ((uint8_t *) p - MEM_NODE_SIZE);

	if (node == NULL_PTR) {
		return 0;
	}

	node->used = 0;
	node = node_merge_next(node);
	node_merge_next(node->prev);

	return (1);
}

void uma_debug_node_size() {
    char node_size_string[256];
    int_to_string(MEM_NODE_SIZE, node_size_string);
    print_string("DYNAMIC_MEM_NODE_SIZE = ");
    print_string(node_size_string);
    print_nl();
}

void print_dynamic_mem_node(struct mem_node *node) {
    char size_string[256];
    int_to_string(node->size, size_string);
    print_string("{size = ");
    print_string(size_string);
    char used_string[256];
    int_to_string(node->used, used_string);
    print_string("; used = ");
    print_string(used_string);
    print_string("}; ");
}

void uma_debug_mem() {
    struct mem_node *node = mem_root;
    print_string("[");
    while (node != NULL_PTR) {
        print_dynamic_mem_node(node);
        node = node->next;
    }
    print_string("]\n");
}
