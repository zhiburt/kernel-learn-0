#ifndef __UMA__
#define __UMA__

#include "stddef.h"
#include "stdint.h"

#define NULL_PTR ((void *)0)

void uma_init();
void *uma_alloc(size_t size);
int uma_free(void* p);

void uma_debug_node_size();
void uma_debug_mem();

#endif